

# Third Challenge
Congratulations ⚔️, you're now the hero of the Customer Success Team and other developer
because of your excellent testing chops. 🙌

The dashboard is function just fine but with so many people applying for funds, it's getting a little slow to load all of the records in each time. The Customer Success Manager comes to you and asks if there's anything that can be done to speed it up. You both decide that adding pagination to the table/API should do the trick.

### Bonus modifications:
- `material-ui` also supports sorting within their library, add sorting
to the table as well so that you can sort by students without payments so there's even less scrolling/page changes.
- Make a proposal: there are a million ways to improve a codebase. Propose a change that could be made to the application that would make it better for either the developer or the "customer" and explain the impact that it would have.

Erik's proposed changes:
- We should use the DataGrid component instead of the table we are using currently - https://mui.com/components/data-grid/#mit-version
	- The setup for pagination is much easier
	- Sorting works by default (and multi column sorting as well I believe, which would help with the "students without payments" modification signficantly)
	- Far less code for doing the same thing we're doing now

- A filter could also work better than a sort if someone only cares about students that need to be paid. We should add the ability to filter rows out of 
our DataGrid

- This is really just a bug that I didn't have a chance to fix, but "Payment Method" and "Initiate Payment" either shouldn't be sortable or need to have a
different comparator in order to work properly


